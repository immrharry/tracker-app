This app serves as a backend for a React Native project
called 'Tracks'

Authentication such as user sign up and sign is built in it, along with customized middleware using ExpressJS, JSON web token, brcypt, MongoDB/mongoose, creating schema for users and tracks (to track and maintain record of location tracks created by
a particular user).

The app is also deployed on heroku by the URL:
https://safe-badlands-24499.herokuapp.com

So in order to run it, you must be running the 'Tracks' app,
whose repository is also available on my bitbucket profile. So kindly run
'Tracks', this here was published with the intention to show code only.

Note: This project was actually built with the help of a Udemy course, and the idea of the project is by no means uniquely produced.
